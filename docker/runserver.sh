#!/bin/bash
cd /srv/mediawiki || exit 1
BIND_TO=$(php -r 'print_r(gethostname()."\n");')
exec php -S "${BIND_TO}:4000"