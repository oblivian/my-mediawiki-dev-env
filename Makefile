CURRENT_UID := $(shell id -u)
USER := $(shell id -un)
image:
	docker build -t mw_devenv:$(shell date +%Y%m%d) docker/
	docker tag mw_devenv:$(shell date +%Y%m%d) mw_devenv:latest

run: check_env
	docker run --user ${CURRENT_UID} --name "mw-devenv-${USER}" -p 4000:4000 -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro -v ${MW_HOME}:/srv/mediawiki:rw mw_devenv:latest


check_env:
ifndef MW_HOME
	@echo "Please define MW_HOME as the full path to your mediawiki/core checkout"
	exit 1
endif

clean:
	docker stop "mw-devenv-${USER}" && docker rm "mw-devenv-${USER}"

PHONY: image run check_env clean